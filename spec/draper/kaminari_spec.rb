require 'rails_helper'

describe Draper::Kaminari do
  before do
    create_list(:person, 10)
    decorator.h.params[:controller] = "people"
  end

  let(:decorator){ Draper::CollectionDecorator.new(people) }
  let(:people){ Person.page(2).per(3) }

  describe "#paginate" do
    it "は、ページ移動リンク群を生成すること" do
      expect(decorator.paginate).to have_tag("nav.pagination"){
        with_tag("span.page", count: people.total_pages)
        with_tag("span.page.current", text: /#{people.current_page}/)

        with_tag("a", with: {href: "/people"})
        with_tag("a", with: {href: "/people?page=3"})
        with_tag("a", with: {href: "/people?page=4"})

        without_tag("a", with: {href: "/people?page=#{people.current_page}"})
      }
    end
  end

  describe "#link_to_previous_page" do
    it "は、前のページへのリンクを生成すること" do
      expect(decorator.link_to_previous_page).to have_tag("a", with: {href: "/people", rel: "prev"})
    end
  end

  describe "#path_to_prev_page" do
    it "は、前のページへのパスを返すこと" do
      expect(decorator.path_to_prev_page).to eq("/people")
    end
  end

  describe "#link_to_next_page" do
    it "は、次のページへのリンクを生成すること" do
      expect(decorator.link_to_next_page).to have_tag("a", with: {href: "/people?page=3", rel: "next"})
    end
  end

  describe "#path_to_next_page" do
    it "は、次のページへのパスを返すこと" do
      expect(decorator.path_to_next_page).to eq("/people?page=3")
    end
  end

  describe "#page_entries_info" do
    it "は、全件数と表示件数を含む文字列を返すこと" do
      expect(decorator.page_entries_info).to eq("Displaying people <b>4&nbsp;-&nbsp;6</b> of <b>10</b> in total")
    end
  end

  describe "#rel_next_prev_link_tags" do
    it "は、<link>要素を生成すること" do
      expect(decorator.rel_next_prev_link_tags).to have_tag("link", with: {href: "/people?page=3", rel: "next"})
    end
  end

  describe "デリゲートしたメソッド全般" do
    describe "オプション`:memo`" do
      context "に`true`を渡した場合 (default)" do
        it "は、結果をメモ化すること" do
          expect(decorator.paginate).to equal(decorator.paginate)
        end
      end

      context "に`false`を渡した場合" do
        it "は、結果をメモ化しないこと" do
          expect(decorator.paginate(memo: false)).not_to equal(decorator.paginate)
        end
      end
    end

    describe "オプション`:wrap`" do
      context "に`false`を渡した場合 (default)" do
        it "は、デリゲート結果を返すこと" do
          expect(decorator.path_to_prev_page).to eq("/people")
        end
      end

      context "に`true`を渡した場合" do
        it "は、デリゲート結果を内容として含む要素を返すこと" do
          expect(decorator.path_to_prev_page(wrap: true)).to have_tag("div.path_to_prev_page", text: "/people")
        end
      end
    end

    context "引数を渡した場合" do
      let(:arg){ double("Argument") }

      it "は、デリゲート先メソッドに引数を渡すこと" do
        expect(decorator.h).to receive(:paginate).with(equal(people), equal(arg), {})
        decorator.paginate(arg)
      end
    end

    context "オプションを渡した場合" do
      it "は、デリゲート先メソッドに所定のキーを除いたオプションを渡すこと" do
        expect(decorator.h).to receive(:paginate).with(equal(people), match(a: "A", b: "B"))
        decorator.paginate(a: "A", b: "B", memo: false, wrap: true)
      end
    end

    context "ブロックを渡した場合" do
      let(:block){ ->{} }

      it "は、デリゲート先メソッドにブロックを渡すこと" do
        expect(decorator.h).to receive(:paginate){|&b| expect(b).to equal(block) }
        decorator.paginate(&block)
      end
    end
  end
end
