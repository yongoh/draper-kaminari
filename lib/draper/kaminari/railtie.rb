module Draper
  module Kaminari
    class Railtie < ::Rails::Railtie

      initializer "include to Draper::CollectionDecorator" do
        Draper::CollectionDecorator.send(:include, Kaminari)
      end
    end
  end
end
