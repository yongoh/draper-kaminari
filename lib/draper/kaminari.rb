require "draper"
require "kaminari"
require "kaminari/helpers/helper_methods"

require "draper/kaminari/railtie"

module Draper
  module Kaminari

    # Kaminariのビューヘルパーへデリゲート
    ::Kaminari::Helpers::HelperMethods.instance_methods.each do |method_name|
      variable_name = "@#{method_name}"

      # @param [Boolean] memo メモ化するかどうか
      # @param [Boolean] wrap デリゲート結果を要素で包むかどうか
      define_method(method_name) do |*args, memo: true, wrap: false, **options, &block|
        if wrap
          result = send(method_name, *args, memo: memo, **options, &block)
          h.content_tag(:div, result, class: method_name.to_s)
        elsif memo
          if instance_variable_defined?(variable_name)
            instance_variable_get(variable_name)
          else
            result = send(method_name, *args, memo: false, **options, &block)
            instance_variable_set(variable_name, result)
          end
        else
          h.send(method_name, object, *args, options, &block)
        end
      end
    end
  end
end
