$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "draper/kaminari/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "draper-kaminari"
  spec.version     = Draper::Kaminari::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = ""
  spec.description = ""
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = ""
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "draper"
  spec.add_dependency "kaminari"

  spec.add_development_dependency "rails"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "database_cleaner"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "rspec-html-matchers"
end
